### What is this repository for? ###

* Pentalog application

* Solved problems http://pastebin.com/W9fRFRgm

### About the app ###

* Used Spring framework for the back-end

* Used AngularJS,HTML,CSS for the front-end

### How do I get set up? ###

* You don't need to run the application because it's already online

* To avoid any problems with internal webserver you can found the application online at http://52.59.239.11:1337/

* All calls are transmitted through HTTP POST from the UI

* The front-end of the application is located at webapp/WEB-INF/WebApp

* The back-end of the application is located at java/com/spring/project

### P.S. ###

* Forgot to delete the services folder in front-end application