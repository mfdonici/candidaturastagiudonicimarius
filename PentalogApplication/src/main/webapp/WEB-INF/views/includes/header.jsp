<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:set var="context" value="${pageContext.request.contextPath}" />
${context}
<link rel="stylesheet" type="text/css" href="<c:url value="${context}/resources/css/design.css"/>">
<html>
<head>
    <title>Spring Project Blueprint</title>
    <h2>Spring project blueprint</h2>
</head>
<body>