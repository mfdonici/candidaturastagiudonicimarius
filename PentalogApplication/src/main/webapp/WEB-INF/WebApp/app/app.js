var myApp = angular.module('myApp', ['ngRoute', 'ngAnimate', "ui.bootstrap.modal"]);
//Application router
myApp.config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
        templateUrl: 'views/home.html'
        , controller: 'mainController'
    }).when('/shape', {
        templateUrl: 'views/shape.html'
        , controller: 'shapeController'
    }).when('/numbers', {
        templateUrl: 'views/numbers.html'
        , controller: 'numbersController'
    }).when('/about', {
        templateUrl: 'views/about.html'
        , controller: 'mainController'
    }).otherwise({
        redirectTo: "/"
    });
    $locationProvider.html5Mode({
        enabled: true
        , requireBase: false
        , rewriteLinks: true
    });
});