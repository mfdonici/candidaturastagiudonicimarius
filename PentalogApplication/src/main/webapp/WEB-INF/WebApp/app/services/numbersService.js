(function(){
    "use strict";
    
    angular.module("myApp").service("numbersService",numbersService);
    
    numbersService.$inject = ["$location","$http"];
    
    function numbersService($location,$http,$scope){
        
        var responseObject = [];
        
        this.evaluateNumbers = function(numbersList){
            var postData = new Object();
            postData.variablesList = numbersList;
            postData.message = "Hallo";
            
            
            $http({
                method: 'POST',
                url: 'http://localhost:1337/api/numbers',
                data: JSON.stringify(postData),
                headers:{
                    'Content-Type': 'application/json'
                }
            }).success(function(response){
                    $scope.responseObject = response;
               });  
        };
        
        this.getResponseObject = function(){
          return $scope.responseObject;  
        };
    }
})();