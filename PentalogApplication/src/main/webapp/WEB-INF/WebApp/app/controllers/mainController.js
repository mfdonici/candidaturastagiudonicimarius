(function(){
    "use strict";
    
    angular.module("myApp").controller("mainController",mainController);
    
    mainController.$inject = ["$location","$scope"];
    
    function mainController($location,$scope){
        
        
        $scope.message = "Technologies used in order to create the application(click on them)";
        
        
        //Popup object for modal
        $scope.popup = new Object();
        
        
        
        //This function opens and updates the modal information based on which image we click.
        $scope.updateModal = function(msg){
            console.log("Trying to update modal");
            if(msg == 'Spring'){
                $scope.popup.header='Spring';
                $scope.popup.message='For the back-end of the application I have used Spring.'+
                '\nI have created a REST service for each problem.The data is sent via Http POST.';
            } 
            else if(msg == 'Angular'){
                $scope.popup.header = 'AngularJS';
                $scope.popup.message = 'For the front-end part of the application I have used AngularJS,HTML,CSS.Through the angular routing I have managed to do a single page application';
            }
            $scope.showModal = true;
        };
        
        
        $scope.closeModal = function(){
            $scope.showModal = false;
        };
    }
})();