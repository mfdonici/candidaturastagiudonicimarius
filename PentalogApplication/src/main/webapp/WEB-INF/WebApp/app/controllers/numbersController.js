(function(){
    "use strict";
    
    angular.module("myApp").controller("numbersController",numbersController);
    
    numbersController.$inject = ["$http","$location","$scope"];
    
    function numbersController($http,$location,$scope){
        
        
        $scope.valuesList = [];
        
        //Init of the responseObject
        $scope.responseObject;
        $scope.errorMessage;
        $scope.message = "Write a comma separeted list of numbers.";
        
        
        //This function submits the user input
        //Sends a HTTP POST request with the postData
        //Receives an object which is delivered to the interface
        $scope.submit = function(){
            $scope.finalList = []
            
            
            //Convert list of strings into list of integers
            for(var i = 0;i < $scope.valuesList.length;i++){
                $scope.finalList.push(+$scope.valuesList[i]);
            }
            
            
            //Create postObject
            var postData = new Object();
            postData.variablesList = $scope.finalList;
            postData.message = "Hallo";
            
            
            
            $http({
                method: 'POST',
                url: 'http://ec2-52-59-239-11.eu-central-1.compute.amazonaws.com:8080/pentalogApplication-1.0/api/numbers',
                data: JSON.stringify(postData),
                headers:{
                    'Content-Type': 'application/json'
                }
            }).success(function(response){
                       $scope.responseObject = response;
            });
        };
        
        
        //Checking if all numbers in the array are numbers
        //Not very elegant
        $scope.inputValidation = function(valuesList){
            $scope.outOfRange = false;
            $scope.notNumbers = false;
            $scope.errorMessage = "";
            
                //Verify if input is clear
                if(valuesList == undefined){
                    $scope.validationError = true;
                    $scope.errorMessage += "You have to write at least a number";
                    
                    return false;
                }
                for(var i = 0; i < valuesList.length; i++){
                    
                    
                    //Check if all the values are numbers
                    if(isNaN(valuesList[i]) == true && $scope.notNumbers == false){
                        
                        $scope.validationError = true;
                        $scope.notNumbers = true;
                        $scope.errorMessage += "Only numbers are allowed.";
                        
                    }
                    
                    //Check if the numbers are going out of range
                    //Not very elegant
                    if((valuesList[i]>9000000000000000 || valuesList[i]<-9000000000000000) && $scope.outOfRange == false){
                        
                        $scope.validationError = true;
                        $scope.outOfRange = true;
                        $scope.errorMessage += "Try to keep it under 9 quadrillion.";
                        
                    }
                    
                    //Check if the input text-areas are empty
                    if(valuesList[i] == "" && $scope.notNumbers == false){
                        
                        $scope.notNumbers = true;
                        $scope.validationError = true;
                        $scope.errorMessage += "You have to fill all the text-areas.";

                    }
                }
                
                
                if($scope.notNumbers == false && $scope.outOfRange == false){
                    $scope.validationError = false;
                    
                    return true;
                }
                
                return false;
            
        };
    }
})();