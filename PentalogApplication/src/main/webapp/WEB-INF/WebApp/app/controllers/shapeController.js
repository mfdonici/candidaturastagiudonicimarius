(function () {
    "use strict";
    angular.module("myApp").controller("shapeController", shapeController);
    shapeController.$inject = ["$location", "$scope", "$http"];

    function shapeController($location, $scope, $http) {
        //Create a field object in order to store the information for each field
        var field = function field(label) {
            this.label = label;
            this.value = "";
        };
        $scope.responseObject;
        $scope.shape = new Object();
        $scope.shape.name;
        $scope.shape.fields = [];
        //Setting the shape variables
        $scope.clearShape = $scope.shape;
        $scope.message = "Please pick a shape(the square is supposed to be a rectangle)";
        $scope.setShape = function (shapeName) {
            //Each time the user sets a new shape the previous shape needs to be "cleaned".
            //I copy the clearShape variable into the $scope.shape in order to do that.
            $scope.shape = angular.copy($scope.clearShape);
            $scope.shape.name = shapeName;
            //Based on the user choice we update the $scope.shape object
            //For example if the user choose the circle the $scope.shape object will have only
            //one field in the fields array
            if ($scope.shape.name == 'Circle') {
                $scope.shape.fields.push(new field("Radius"));
            }
            if ($scope.shape.name == 'Rectangle') {
                $scope.shape.fields.push(new field("Width"));
                $scope.shape.fields.push(new field("Length"));
            }
            if ($scope.shape.name == 'Triangle') {
                $scope.shape.fields.push(new field("A"));
                $scope.shape.fields.push(new field("B"));
                $scope.shape.fields.push(new field("C"));
            }
            //Reset the response object in order to clear interface
            $scope.responseObject = "";
            $scope.validationError = false;
            //I have called a validation before to be sure that the user fills all the text-areas
            $scope.inputValidation();
        };
        $scope.evaluate = function () {
            var finalList = [];
            var fields = $scope.shape.fields;
            //Convert list of strings into list of integers
            for (var i = 0; i < fields.length; i++) {
                finalList.push(+fields[i].value);
            }
            //Create the post object
            var postData = new Object();
            postData.variablesList = finalList;
            postData.message = "Hallo";
            $http({
                method: 'POST'
                , url: 'http://ec2-52-59-239-11.eu-central-1.compute.amazonaws.com:8080/pentalogApplication-1.0/api/shapeCalculator'
                , data: JSON.stringify(postData)
                , headers: {
                    'Content-Type': 'application/json'
                }
            }).success(function (response) {
                $scope.responseObject = response;
            });
        };
        //This function  modifies the classes of the shape.
        $scope.selectMe = function (event) {
            $("a.shape").removeClass("selected");
            $(event.target).addClass('selected');
        };
        //Validation function
        $scope.inputValidation = function () {
            $scope.errorMessage = "";
            //I have set these variables to return each error type if needed.
            var negativeNumbers = false;
            var notANumber = false;
            var triangleConstraint = false;
            var fields = $scope.shape.fields;
            var checkTriangleConstraint = function (a, b, c) {
                if ((a + c) <= b || (a + b) <= c || (c + b) <= a) {
                    $scope.validationError = true;
                    triangleConstraint = true;
                    $scope.errorMessage += "Triangle inequality not respected";
                }
            };
            if (fields.length == 3 && triangleConstraint == false) {
                checkTriangleConstraint(+fields[0].value, +fields[1].value, +fields[2].value);
            }
            
            for (var i = 0; i < fields.length; i++) {
                //Check if all the inputs are positive numbers
                if (fields[i].value < 0 && negativeNumbers == false) {
                    negativeNumbers = true;
                    $scope.validationError = true;
                    $scope.errorMessage += "You can only have positive numbers.";
                }
                //Check if all the inputs are numbers
                if (isNaN(fields[i].value) && notANumber == false) {
                    notANumber = true;
                    $scope.validationError = true;
                    $scope.errorMessage += "You must have only numbers."
                }
                //Check if the input text-areas are empty
                if (fields[i].value == "" && notANumber == false) {
                    notANumber = true;
                    $scope.validationError = true;
                    $scope.errorMessage += "You have to fill all the text-areas.";
                }
            }
            if (negativeNumbers == false && notANumber == false && triangleConstraint == false) {
                $scope.validationError = false;
                return true;
            }
            return false;
        };
    }
})();