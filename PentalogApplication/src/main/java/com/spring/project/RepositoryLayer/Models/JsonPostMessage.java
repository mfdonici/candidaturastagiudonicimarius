package com.spring.project.RepositoryLayer.Models;

import java.util.List;

/**
 * Created by donic on 1/21/2017.
 */
public class JsonPostMessage {

    private List<Long> variablesList;

    public JsonPostMessage() {
    }

    public List<Long> getVariablesList() {
        return variablesList;
    }

    public void setVariablesList(List<Long> variablesList) {
        this.variablesList = variablesList;
    }

}
