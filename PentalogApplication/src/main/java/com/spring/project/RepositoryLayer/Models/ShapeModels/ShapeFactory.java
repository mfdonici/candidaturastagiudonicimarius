package com.spring.project.RepositoryLayer.Models.ShapeModels;

import java.util.List;

/**
 * Created by donic on 1/21/2017.
 */
public class ShapeFactory {

    //I have created a shape factory based on the number of values in the given array
    //I could have done this easily with the shape name which was received through the
    //POST message

    public Shape getShape(List<Long> valueList) {
        int listSize = valueList.size();
        if (listSize == 0 || listSize > 3) {
            return null;
        } else if (listSize == 1) {
            double radius = valueList.get(0);

            return new Circle(radius);
        } else if (listSize == 2) {
            double width = valueList.get(0);
            double length = valueList.get(1);

            return new Rectangle(width, length);
        } else if (listSize == 3) {
            double a = valueList.get(0);
            double b = valueList.get(1);
            double c = valueList.get(2);

            return new Triangle(a, b, c);
        }

        return null;
    }


}
