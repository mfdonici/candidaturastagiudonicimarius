package com.spring.project.RepositoryLayer.Models.ShapeModels;


/**
 * Created by donic on 1/21/2017.
 */
public class Rectangle implements Shape {
    private double width, length;
    private String shapeName = "RECTANGLE";

    public Rectangle(double width, double length) {
        this.width = width;
        this.length = length;
    }

    @Override
    public double area() {
        double areaValue = width * length;

        return areaValue;
    }

    @Override
    public double perimeter() {
        double perimeterValue = width * 2 + length * 2;

        return perimeterValue;
    }

    @Override
    public String getShapeName() {
        return shapeName;
    }
}
