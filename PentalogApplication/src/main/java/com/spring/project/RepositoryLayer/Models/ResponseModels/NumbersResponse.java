package com.spring.project.RepositoryLayer.Models.ResponseModels;

import java.util.Set;

/**
 * Created by donic on 1/21/2017.
 */
public class NumbersResponse {

    private Set<Long> primeNumbers;
    private Set<Long> palindromeNumbers;
    private long smallestNumber;
    private long largestNumber;

    public Set<Long> getPrimeNumbers() {
        return primeNumbers;
    }

    public void setPrimeNumbers(Set<Long> primeNumbers) {
        this.primeNumbers = primeNumbers;
    }

    public Set<Long> getPalindromeNumbers() {
        return palindromeNumbers;
    }

    public void setPalindromeNumbers(Set<Long> palindromeNumbers) {
        this.palindromeNumbers = palindromeNumbers;
    }

    public long getSmallestNumber() {
        return smallestNumber;
    }

    public void setSmallestNumber(long smallestNumber) {
        this.smallestNumber = smallestNumber;
    }

    public long getLargestNumber() {
        return largestNumber;
    }

    public void setLargestNumber(long largestNumber) {
        this.largestNumber = largestNumber;
    }

    @Override
    public String toString() {
        return "NumbersResponse{" +
                "primeNumbers=" + primeNumbers +
                ", palindromeNumbers=" + palindromeNumbers +
                ", smallestNumber=" + smallestNumber +
                ", largestNumber=" + largestNumber +
                '}';
    }
}
