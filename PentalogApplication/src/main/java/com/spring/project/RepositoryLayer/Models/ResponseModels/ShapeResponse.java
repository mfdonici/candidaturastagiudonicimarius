package com.spring.project.RepositoryLayer.Models.ResponseModels;

/**
 * Created by donic on 1/21/2017.
 */
public class ShapeResponse {
    private String message;
    private double area;
    private double perimeter;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public double getArea() {
        return area;
    }

    public void setArea(double area) {
        this.area = area;
    }

    public double getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(double perimeter) {
        this.perimeter = perimeter;
    }
}
