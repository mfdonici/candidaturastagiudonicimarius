package com.spring.project.RepositoryLayer.Implementation;

import com.spring.project.RepositoryLayer.Interfaces.INumbersRepository;
import org.springframework.stereotype.Repository;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by donic on 1/21/2017.
 */
@Repository("numbersRepository")
public class NumbersImpl implements INumbersRepository {
    List<Long> numbersList;

    public NumbersImpl() {
    }

    @Override
    public long getSmallestNumber() {

        if (numbersList != null) {
            long smallestNumber = numbersList.get(0);
            for (int i = 1; i < numbersList.size(); i++) {
                if (smallestNumber > numbersList.get(i)) {
                    smallestNumber = numbersList.get(i);
                }
            }
            return smallestNumber;
        }

        //The input from is validated in the front-end so the user cannot send an empty list
        //I still had to put this if someone send a POST request but not through the application
        //Random large number :(
        return 1000000000;
    }

    @Override
    public long getLargestNumber() {
        if (numbersList != null) {
            long largestNumber = numbersList.get(0);
            for (int i = 1; i < numbersList.size(); i++) {
                if (largestNumber < numbersList.get(i)) {
                    largestNumber = numbersList.get(i);
                }
            }
            return largestNumber;
        }

        //The input from is validated in the front-end so the user cannot send an empty list
        //I still had to put this if someone send a POST request but not through the application
        //Random small number :(
        return -1000000000;
    }

    public boolean checkPrime(long number) {
        boolean flag = true;
        long topMargin = number / 2;

        for (int i = 2; i <= topMargin; i++) {
            if (number % i == 0) {
                flag = false;
                break;
            }
        }

        return flag;
    }

    public boolean checkPalindrome(long number) {
        long reverseNumber = 0, aux = number;
        if (number < 0) aux *= -1;
        while (aux > 0) {
            reverseNumber = reverseNumber * 10 + aux % 10;
            aux /= 10;
        }
        if (number < 0) reverseNumber *= -1;
        if (reverseNumber == number)
            return true;

        return false;
    }

    @Override
    public Set<Long> getPrimeNumbers() {
        Set<Long> primeNumbers = new HashSet<Long>();

        for (long number : numbersList) {
            if (checkPrime(number)) {
                primeNumbers.add(number);
            }
        }

        return primeNumbers;
    }

    @Override
    public Set<Long> getPalindromeNumbers() {
        Set<Long> palindromeNumbers = new HashSet<Long>();

        for (long number : numbersList) {
            if (checkPalindrome(number)) {
                palindromeNumbers.add(number);
            }
        }

        return palindromeNumbers;
    }

    @Override
    public void initNumberList(List<Long> valueList) {
        if (valueList.size() > 0)
            numbersList = valueList;
    }
}
