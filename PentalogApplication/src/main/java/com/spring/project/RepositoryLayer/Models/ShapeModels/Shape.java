package com.spring.project.RepositoryLayer.Models.ShapeModels;

/**
 * Created by donic on 1/21/2017.
 */
public interface Shape {
    public double area();

    public double perimeter();

    public String getShapeName();
}
