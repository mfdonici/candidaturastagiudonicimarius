package com.spring.project.RepositoryLayer.Models.ShapeModels;

/**
 * created by donic on 1/21/2017.
 */
public class Triangle implements Shape {
    private double a, b, c;
    private String shapeName = "TRIANGLE";

    public Triangle(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    @Override
    public double area() {
        //      Heron's formula
        //      sqrt(sValue * (sValue - a) * (sValue - b) * (sValue - c))


        //Semiperimeter
        double sValue = (a + b + c) / 2;

        double valueTobeSquared = sValue * (sValue - a) * (sValue - b) * (sValue - c);

        double areaValue = Math.sqrt(valueTobeSquared);

        return areaValue;

    }

    @Override
    public double perimeter() {
        double perimeterValue = a + b + c;

        return perimeterValue;
    }

    @Override
    public String getShapeName() {
        return shapeName;
    }
}
