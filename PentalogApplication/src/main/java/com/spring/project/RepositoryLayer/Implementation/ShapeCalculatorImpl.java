package com.spring.project.RepositoryLayer.Implementation;

import com.spring.project.RepositoryLayer.Interfaces.IShapeCalculatorRepository;
import com.spring.project.RepositoryLayer.Models.ShapeModels.Shape;
import com.spring.project.RepositoryLayer.Models.ShapeModels.ShapeFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by donic on 1/21/2017.
 */
@Repository("calculatorRepository")
public class ShapeCalculatorImpl implements IShapeCalculatorRepository {

    private Shape chosenShape;
    private ShapeFactory shapeFactory = new ShapeFactory();

    public ShapeCalculatorImpl() {
    }


    public String initShape(List<Long> valueList) {
        chosenShape = shapeFactory.getShape(valueList);
        if (chosenShape != null)
            return chosenShape.getShapeName();
        return "Now a shape";
    }

    @Override
    public double calculateArea() {
        double areaValue = -1;
        if (this.chosenShape != null)
            areaValue = chosenShape.area();

        return areaValue;
    }

    @Override
    public double calculatePerimeter() {
        double perimeterValue = -1;
        if (this.chosenShape != null)
            perimeterValue = chosenShape.perimeter();
        return perimeterValue;
    }

}
