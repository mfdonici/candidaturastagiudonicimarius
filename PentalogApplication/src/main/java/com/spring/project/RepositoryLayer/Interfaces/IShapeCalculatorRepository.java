package com.spring.project.RepositoryLayer.Interfaces;

import java.util.List;

/**
 * Created by donic on 1/21/2017.
 */
public interface IShapeCalculatorRepository extends IGenericRepository {

    double calculateArea();

    double calculatePerimeter();

    String initShape(List<Long> valueList);
}
