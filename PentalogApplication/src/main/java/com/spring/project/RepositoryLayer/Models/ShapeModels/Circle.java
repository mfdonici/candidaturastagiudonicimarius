package com.spring.project.RepositoryLayer.Models.ShapeModels;

/**
 * Created by donic on 1/21/2017.
 */
public class Circle implements Shape {
    private double radius;
    private String shapeName = "CIRCLE";
    final double pi = Math.PI;

    public Circle(double radius) {
        this.radius = radius;
    }


    @Override
    public double area() {
        double areaValue = pi * Math.pow(radius, 2);

        return areaValue;
    }

    @Override
    public double perimeter() {
        double perimeterValue = 2 * pi * radius;

        return perimeterValue;
    }

    @Override
    public String getShapeName() {
        return shapeName;
    }
}
