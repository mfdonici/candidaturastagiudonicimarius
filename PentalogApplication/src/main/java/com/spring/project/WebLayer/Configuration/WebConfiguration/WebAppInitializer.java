package com.spring.project.WebLayer.Configuration.WebConfiguration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * Created by donic on 12/1/2016.
 */
public class WebAppInitializer implements WebApplicationInitializer {

    private static final Logger logger = LoggerFactory.getLogger(WebAppInitializer.class);

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();

        //Dispatcher servlet config
        AnnotationConfigWebApplicationContext mvcContext = new AnnotationConfigWebApplicationContext();
        mvcContext.register(WebMvcConfig.class);

        ServletRegistration.Dynamic register = servletContext.addServlet("dispatcher", new DispatcherServlet(mvcContext));

        //Set the parameter to true to be able to handle the NoHandlerFound exception
        //register.setInitParameter("throwExceptionIfNoHandlerFound", "true");


        register.setLoadOnStartup(1);
        register.addMapping("/");
    }
}
