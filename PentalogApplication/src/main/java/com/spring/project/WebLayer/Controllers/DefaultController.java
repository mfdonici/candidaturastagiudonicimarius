package com.spring.project.WebLayer.Controllers;

import com.spring.project.BusinessLayer.Interfaces.IShapeCalculatorService;
import com.spring.project.BusinessLayer.Interfaces.INumbersService;
import com.spring.project.RepositoryLayer.Models.JsonPostMessage;
import com.spring.project.RepositoryLayer.Models.ResponseModels.NumbersResponse;
import com.spring.project.RepositoryLayer.Models.ResponseModels.ShapeResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * Created by donic on 12/1/2016.
 */
@RestController
public class DefaultController {

    @Autowired
    IShapeCalculatorService calculatorService;

    @Autowired
    INumbersService numbersService;

    @RequestMapping(value = "/api/hello/{name}")
    public String message(@PathVariable String name){

        String message = "Hello " + name + " !";
        return message;
    }


    @CrossOrigin
    @RequestMapping(value = "/api/shapeCalculator",method = RequestMethod.POST , consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody ShapeResponse shapeCalculator(@RequestBody JsonPostMessage jsonMessage){

        List<Long> valueList =  jsonMessage.getVariablesList();
        ShapeResponse response = new ShapeResponse();
        //Set shape based on the number of values in the valueList
        //1-Circle      2-Rectangle     3-Triangle
        String evaluateShape = calculatorService.initShape(valueList);

        //Get the areaValue and perimeterValue from the service()
        double areaValue = calculatorService.calculateArea();
        double perimeterValue = calculatorService.calculatePerimeter();

        //Set the response for the api
        response.setMessage(evaluateShape);
        response.setArea(areaValue);
        response.setPerimeter(perimeterValue);

        //Return the response
        return response;
    }

    @CrossOrigin
    @RequestMapping(value = "/api/numbers",method = RequestMethod.POST , consumes = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody NumbersResponse numbers(@RequestBody JsonPostMessage jsonMessage){
        List<Long> valueList = jsonMessage.getVariablesList();

        NumbersResponse response = new NumbersResponse();

        //Initialize the list of values;
        numbersService.initNumberList(valueList);

        //Get all the variables we need from the service.
        //I could have done this directly in the service.
        long smallestNumber = numbersService.getSmallestNumber();
        long largestNumber = numbersService.getLargestNumber();
        Set<Long> primeNumbers = numbersService.getPrimeNumbers();
        Set<Long> palindromeNumbers = numbersService.getPalindromeNumbers();

        //Set the response variables
        response.setLargestNumber(largestNumber);
        response.setSmallestNumber(smallestNumber);
        response.setPalindromeNumbers(palindromeNumbers);
        response.setPrimeNumbers(primeNumbers);

        System.out.println(response);
        //Returning the response to the application
        return response;
    }


}