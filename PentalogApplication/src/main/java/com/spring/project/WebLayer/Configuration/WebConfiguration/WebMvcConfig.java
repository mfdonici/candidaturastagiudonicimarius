package com.spring.project.WebLayer.Configuration.WebConfiguration;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

/**
 * Created by donic on 12/1/2016.
 */

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"com.spring.project.WebLayer.Controllers",
        "com.spring.project.BusinessLayer.Implementation",
        "com.spring.project.RepositoryLayer.Implementation",
        "com.spring.project.WebLayer.WebConfiguration"})
public class WebMvcConfig extends WebMvcConfigurerAdapter {
    private static final String VIEWS = "/WEB-INF/views/";

    private static final String RESOURCE_LOCATION = "/resources/";
    private static final String RESOURCE_HANDLER = "/resources/**";

    private static final Logger logger = LoggerFactory
            .getLogger(WebMvcConfig.class);


    @Bean
    public ViewResolver viewResolver() {
        logger.debug("Setting up view resolver");

        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix(VIEWS);
        viewResolver.setSuffix(".jsp");

        return viewResolver;
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        logger.debug("Setting up resource handler");

        registry.addResourceHandler(RESOURCE_HANDLER).addResourceLocations(RESOURCE_LOCATION);
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }
}
