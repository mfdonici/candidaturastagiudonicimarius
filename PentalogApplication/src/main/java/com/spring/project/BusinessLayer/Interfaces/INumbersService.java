package com.spring.project.BusinessLayer.Interfaces;

import java.util.List;
import java.util.Set;

/**
 * Created by donic on 1/21/2017.
 */
public interface INumbersService {
    long getSmallestNumber();

    long getLargestNumber();

    Set<Long> getPrimeNumbers();

    Set<Long> getPalindromeNumbers();

    void initNumberList(List<Long> valueList);
}
