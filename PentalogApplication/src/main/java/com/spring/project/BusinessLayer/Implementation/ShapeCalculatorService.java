package com.spring.project.BusinessLayer.Implementation;

import com.spring.project.BusinessLayer.Interfaces.IShapeCalculatorService;
import com.spring.project.RepositoryLayer.Interfaces.IShapeCalculatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by donic on 1/21/2017.
 */
@Service("calculatorService")

public class ShapeCalculatorService implements IShapeCalculatorService {
    @Autowired
    IShapeCalculatorRepository calculatorRepository;

    public ShapeCalculatorService() {
    }

    public String initShape(List<Long> valueList) {
        String response = calculatorRepository.initShape(valueList);

        return response;
    }

    @Override
    public double calculateArea() {
        double areaValue = calculatorRepository.calculateArea();

        return areaValue;
    }

    @Override
    public double calculatePerimeter() {
        double perimeterValue = calculatorRepository.calculatePerimeter();

        return perimeterValue;
    }
}
