package com.spring.project.BusinessLayer.Interfaces;

import java.util.List;

/**
 * Created by donic on 1/21/2017.
 */
public interface IShapeCalculatorService extends IGenericService {
    public double calculateArea();

    public double calculatePerimeter();

    public String initShape(List<Long> valueList);

}
