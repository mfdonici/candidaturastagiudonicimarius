package com.spring.project.BusinessLayer.Implementation;

import com.spring.project.BusinessLayer.Interfaces.INumbersService;
import com.spring.project.RepositoryLayer.Interfaces.INumbersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

/**
 * Created by donic on 1/21/2017.
 */

@Service("numbersService")
public class NumbersService implements INumbersService {
    @Autowired
    INumbersRepository numbersRepository;

    public NumbersService() {
    }

    @Override
    public long getSmallestNumber() {
        long smallestNumber = numbersRepository.getSmallestNumber();

        return smallestNumber;
    }

    @Override
    public long getLargestNumber() {
        long largestNumber = numbersRepository.getLargestNumber();

        return largestNumber;
    }

    @Override
    public Set<Long> getPrimeNumbers() {
        Set<Long> primeNumbers = numbersRepository.getPrimeNumbers();

        return primeNumbers;

    }

    @Override
    public Set<Long> getPalindromeNumbers() {
        Set<Long> palindromeNumbers = numbersRepository.getPalindromeNumbers();

        return palindromeNumbers;
    }

    @Override
    public void initNumberList(List<Long> valueList) {
        numbersRepository.initNumberList(valueList);
    }
}
